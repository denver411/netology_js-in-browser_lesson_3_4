'use strict';


document.addEventListener('DOMContentLoaded', () => {
  const chatContainer = document.querySelector('.chat');
  const sendMessageText = chatContainer.querySelector('.message-input');
  const sendMessageButton = chatContainer.querySelector('.message-submit');
  const messageArea = chatContainer.querySelector('.messages-content');
  const messagesTemplates = chatContainer.querySelector('.messages-templates');
  const receivedMessage = messagesTemplates.children[1];
  const sentMessage = messagesTemplates.children[2];
  const loadMessage = messagesTemplates.children[0];

  const connection = new WebSocket('wss://neto-api.herokuapp.com/chat');

  function showMessage(messageType, event) {
    let messageTime = new Date(Date.now());
    let newMessage = messageType.cloneNode(true);
    messageArea.appendChild(newMessage);
    messageArea.lastChild.querySelector('.message-text').innerText = event.data;
    messageArea.lastChild.querySelector('.timestamp').innerText = `${messageTime.getHours()}:${messageTime.getMinutes()}`;
  }

  connection.addEventListener('open', () => {
    chatContainer.querySelector('.message-submit').removeAttribute('disabled');
    chatContainer.querySelector('.chat-status').innerText = chatContainer.querySelector('.chat-status').dataset.online;
    console.log('Вебсокет-соединение открыто');
  });

  sendMessageButton.addEventListener('click', event => {
    event.preventDefault();
    connection.send(sendMessageText.value);
    event.data = sendMessageText.value;
    sendMessageText.value = '';
    showMessage(sentMessage, event);
  })

  connection.addEventListener('message', (event) => {
    setTimeout(() => {
      messageArea.removeChild(messageArea.lastChild);
      showMessage(receivedMessage, event);
    }, 1000);
    let newMessage = loadMessage.cloneNode(true);
    messageArea.appendChild(newMessage);
  });

  connection.addEventListener('close', () => {
    chatContainer.querySelector('.message-submit').setAttribute('disabled', '');
    chatContainer.querySelector('.chat-status').innerText = chatContainer.querySelector('.chat-status').dataset.offline;
  });



});