'use strict';

document.addEventListener('DOMContentLoaded', () => {
  const connection = new WebSocket('wss://neto-api.herokuapp.com/counter');

  connection.addEventListener('message', event => {
    let message = JSON.parse(event.data);
    document.querySelector('.counter').innerText = message.connections;
    document.querySelector('output.errors').innerText = message.errors;
  });

  window.addEventListener('beforeunload', () => {
    connection.close(1000)
  });

});