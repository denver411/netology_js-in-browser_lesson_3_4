'use strict';

document.addEventListener('DOMContentLoaded', () => {
  const connection = new WebSocket('wss://neto-api.herokuapp.com/mouse');


  connection.addEventListener('open', () => {
    console.log('Вебсокет-соединение открыто');
  });

  document.addEventListener('click', (event) => {
    console.log(event.pageX, event.pageY);
    connection.send(JSON.stringify({
      x: event.pageX,
      y: event.pageY
    }));
    showBubbles(connection);
  });
});